;;; ett.el --- A simple yet powerful time tracker -*- lexical-binding: t -*-

;; Copyright (C) 2022-2023 John Task

;; Author: John Task <q01@disroot.org>
;; Maintainer: John Task <q01@disroot.org>
;; Version: 4.1
;; Package-Requires: ((emacs "28.1"))
;; Keywords: calendar
;; URL: https://gitlab.com/q01_code/ett

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Emacs Time Tracker (or short ETT) is a simple yet powerful time
;; tracker for Emacs. Even though it's based on a minimalist plain
;; text file, it can show statistics for current day, week, month or
;; year, and even compare tags recording for the same item given any
;; of these periods.
;;
;; Advanced features include percentages, graphs and icons.
;;
;; Clock-in with M-x ett-add-track, go to file with M-x ett-find-file,
;; and get report with M-x ett-report. You probably want to bind these
;; functions to easy keys.

;;; Code:

(require 'transient)
(require 'calendar)
(require 'time-date)

(declare-function all-the-icons-material nil)

(defgroup ett nil
  "Emacs Time Tracker.
A simple yet powerful time tracker for Emacs."
  :tag "ETT"
  :group 'applications)

(defcustom ett-items nil
  "Items for ETT.
Items can contain anything but whitespaces. So, use single words.
Don't worry if you don't set this. ETT doesn't require it to work."
  :type '(repeat :tag "List of items" (string :tag "Item"))
  :group 'ett)

(defcustom ett-icon-alist nil
  "Alist storing icons for ETT items."
  :type '(alist :tag "List of associated icons for items"
                :key-type (string :tag "Item")
                :value-type (string :tag "Icon")))

(defcustom ett-tags nil
  "Tags for ETT.
Tags, unlike items, can be any string.
Setting this variable can result on imprecise completions."
  :type '(repeat :tag "List of tags" (string :tag "Tag")))

(defcustom ett-excluded-items nil
  "List storing items never counted for statistic generation."
  :type '(repeat :tag "Excluded items" (string :tag "Item")))

(defcustom ett-separator "--->"
  "Separator used by ETT.
Don't change this unless you have a good reason to do so.
If you really want to change it, use a unique enough string,
because this separator is used for knowing which lines should
be counted for statistic generation.
Something like '-->' or '====>' would be acceptable."
  :type '(string))

(defcustom ett-pretty-separator "─────>"
  "Separator used for statistic generation buffers.
Unlike `ett-separator', you can safely change this to anything you want."
  :type '(string))

(defcustom ett-file (locate-user-emacs-file "tracks.ett")
  "File for storing ETT tracks."
  :type '(file))

(defcustom ett-use-icons nil
  "Whether ETT should use icons.
You'll need the all-the-icons package for this."
  :type '(boolean))

(defcustom ett-icon-function #'all-the-icons-material
  "Function used to retrieve icons from all-the-icons."
  :type '(function))

(defcustom ett-transient-for-tag "Tags"
  "Keyword for creating a tag on an ETT transient."
  :type '(string))

(defcustom ett-clock-in-hook nil
  "Hook run after calling `ett-add-track'."
  :type '(hook))

(defcustom ett-stop-external-clocks-hook nil
  "Hook run after calling `ett-add-track' interactively.

This hook is useful for instructing ETT to stop any Org clock
when adding a new track, hence the name.  This wouldn't be
possible with `ett-clock-in-hook' if a function derived from
`ett-add-track' also happened to be in `org-clock-in-hook' (which
is the recommended workflow), because in that case the Org clock
would stop inmediately after being started."
  :type '(hook))

(defcustom ett-daily-goal-reached-hook nil
  "Hook run after a daily goal has been reached.

Note that this hook is only run if `ett-modeline-mode' is enabled.

Functions in this hook are called with a single argument, the item
which goal has been reached, as a string."
  :type '(hook))

(defcustom ett-goals nil
  "List of daily, weekly, monthly and yearly goals for ETT.
Each element has the form (ITEM SCOPE MINUTES).
For instance, (\"Read\" day 15) refers to a goal for reading
at least 15 minutes per day."
  :type '(alist :value-type (group symbol integer)))

(defcustom ett-graph-height 10
  "Number of characters ETT should use for graphs.
More characters means more precision.
Of course, it also means the graph will require more screen
size to be displayed.
Only even numbers are allowed. Indeed, it's a weird bug."
  :type '(natnum))

(defcustom ett-graph-icon ?│
  "Icon used for building ETT graphs."
  :type 'character)

(defface ett-heading
  '((((class color) (min-colors 88) (background light))
     (:foreground "Blue1" :bold t :underline t))
    (((class color) (min-colors 88) (background dark))
     (:foreground "LightSkyBlue" :bold t :underline t))
    (((class color) (min-colors 16) (background light))
     (:foreground "Blue" :bold t :underline t))
    (((class color) (min-colors 16) (background dark))
     (:foreground "LightSkyBlue" :bold t :underline t))
    (((class color) (min-colors 8)) (:foreground "blue" :bold t))
    (t (:bold t)))
  "Face used for ETT heading.")

(defface ett-separator-face
  '((t (:inherit font-lock-constant-face)))
  "Face used for various ETT separators.")

(defvar ett-font-lock-keywords
  (list (list ett-separator '(0 'ett-separator-face))
        (list "^-.*" '(0 'ett-separator-face))
        (list "^[0-9]+-[0-9]+-[0-9]+$" '(0 'bold))
        (list "\\[" '(0 'ett-separator-face))
        (list "\\]" '(0 'ett-separator-face)))
  "Keywords for syntax highlighting on ETT file.")

(defvar ett-view-font-lock-keywords
  (list (list ett-pretty-separator '(0 'ett-separator-face))
        (list "\\[" '(0 'ett-separator-face))
        (list "\\]" '(0 'ett-separator-face))
        (list "^This .*:" '(0 'ett-heading))
        (list "^Date:  .*" '(0 'ett-heading))
        (list "^Period:  .*" '(0 'ett-heading))
        (list "\\(remaining\\) for reaching goal\\]$" '(1 'warning))
        (list "Goal \\(reached\\)\\]$" '(1 'success))
        (list "^\\(Total\\): " '(1 'underline))
        (list "^\\(Average\\): " '(1 'underline))
        (list "^\\(Most time .*\\): " '(1 'underline))
        (list "^\\(Less time .*\\): " '(1 'underline))
        (list "^\\(Tags report\\):$" '(1 'underline))
        (list "^\\(Pending goals\\):$" '(1 'underline))
        (list (concat (string ett-graph-icon) "+") '(0 'ett-separator-face)))
  "Keywords for syntax highlighting on ETT report buffer.")

(defvar ett--item-history nil
  "Minibuffer history of ETT items.")

(defvar ett--tag-history nil
  "Minibuffer history of ETT tags.")

(defvar ett-items-internal nil)

(defun ett-complete (prompt set)
  "Ask for user input with PROMPT for SET.
PROMPT must be a string.
SET can be the symbol `items' or an item for tag search."
  (let (olmap item tag)
    ;; This is a hack; SPC can be used on tags, so we override
    ;; minibuffer keymap here
    (setq olmap minibuffer-local-completion-map)
    (define-key minibuffer-local-completion-map " " nil)
    (prog1
        (cond ((eq set 'items)
               (setq
                item
                (completing-read
                 prompt
                 (or ett-items (ett-return-items))
                 nil nil nil 'ett--item-history)))
              (t
               (setq
                tag
                (completing-read
                 prompt
                 (if ett-tags
                     ett-tags
                   (with-current-buffer (find-file-noselect ett-file)
                     (ett-parse-tags set))
                   (mapcar
                    #'car
                    (assoc-delete-all "No tags" ett-items-internal)))
                 nil nil nil 'ett--tag-history))))
      ;; Add to history
      (cond (item (add-to-history 'ett--item-history item))
            (tag (add-to-history 'ett--tag-history tag)))
      ;; Restore keymap
      (setq minibuffer-local-completion-map olmap))))

(defun ett-space-dwim ()
  "Insert a space or a time separator indicator."
  (interactive)
  (let ((pos (point))
        citem)
    (cond ((looking-back ":[0-9]\\{2\\}" (- pos 3))
           (insert " " ett-separator " " (ett-complete "Item: " 'items))
           (save-excursion
             (unless (search-forward ett-separator nil t)
               ;; Only check day if there aren't more tracks after point,
               ;; for preventing annoyances
               (ett-ensure-day))))
          ((looking-back "^[0-9]\\{2\\}" (- pos 3))
           (insert ":"))
          ((looking-back " " (- pos 1))
           (save-excursion
             (forward-char -1)
             (skip-chars-backward "^ " (line-beginning-position))
             (setq citem (ett-item-at-point)))
           (insert "[" (ett-complete "Tag: " citem) "]"))
          (t
           (insert " ")))))

;;;###autoload
(defun ett-find-file ()
  "Find current ETT file."
  (interactive)
  (find-file ett-file))

;;;###autoload
(defun ett-add-track (item &optional tag end-external)
  "Track ITEM on `ett-file'. With TAG, also record a tag.

Interactively, ITEM is read from minibuffer, while TAG is read
only when a prefix arg is present.

END-EXTERNAL, when non-nil (interactively, that is), means call
any function found in `ett-stop-external-clocks-hook' upon
completion."
  (interactive
   (let ((it (ett-complete "Item: " 'items)))
     (list it (and current-prefix-arg (ett-complete "Tag: " it)) t)))
  (with-current-buffer (find-file-noselect ett-file)
    (goto-char (point-max))
    (insert (format-time-string "%H:%M")
            " " ett-separator " " item
            (if tag (concat " [" tag "]") ""))
    (ett-ensure-day)
    (save-buffer 0)
    (run-hooks 'ett-clock-in-hook)
    (and end-external (run-hooks 'ett-stop-external-clocks-hook))
    (message "Tracking \"%s\"" item)))

(defalias 'ett-clock-in #'ett-add-track)

(defun ett-ensure-day ()
  "Ensure we are not on a new day. If we are, insert a separator."
  (let ((today (format-time-string "%d-%m-%y")))
    (save-excursion
      (unless (search-backward today nil t)
        ;; When there's no current date, then we just insert it right away.
        ;; A bit extreme, but if we don't do so, everything else will fail.
        ;; Please, respect the specifications.
        (beginning-of-line)
        (insert (if (not (bobp)) "\n" "") today "\n--------\n\n")))))

(defun ett-convert-date (date)
  "Turn DATE (a string) into a usable date for ETT.
Returns a DD-MM-YY date as a string.
Here are some translations (*starred* values are current at the time):

03-02-23 ---> 03-02-23
3-2-23   ---> 03-02-23
3-02     ---> 03-02-*23*
3-2      ---> 03-02-*23*
03       ---> 03-*02*-*23*
3        ---> 03-*02*-*23*"
  (setq date
	(pcase (length date)
	  ;; DD-MM-YY
	  (8 date)
	  ;; DD-MM
	  (5 (concat date (format-time-string "-%y")))
	  ;; DD
	  (2 (concat date (format-time-string "-%m-%y")))
	  ;; D
	  (1 (concat "0" date (format-time-string "-%m-%y")))
	  ;; DD-M
	  (4
	   ;; Or D-MM?
	   (pcase (string-search "0" date)
	     (2 (concat "0" date (format-time-string "-%y")))
	     (_ (concat
                 (replace-regexp-in-string "-\\(.*\\)" "-0\\1" date)
                 (format-time-string "-%y")))))
	  ;; D-M
	  (3 (concat "0" (replace-regexp-in-string "-\\(.*\\)" "-0\\1" date)
		     (format-time-string "-%y")))
	  ;; D-MM-YY
	  (7 ;; Or DD-M-YY?
	   (pcase (string-search "0" date)
	     (2 (concat "0" date))
	     (_ (concat
		 (replace-regexp-in-string "-\\(.*\\)-" "-0\\1-" date)))))
	  ;; D-M-YY (who writes like that?)
	  (6 (concat "0"
		     (replace-regexp-in-string "-\\(.*\\)-" "-0\\1-" date))))))

(defun ett-custom-date (date &optional date2)
  "Operate on current buffer so the only date is DATE.
With optional DATE2, include every date between DATE and DATE2."
  (if (not (stringp date)) (error "Date not defined"))
  (goto-char (point-min))
  (save-excursion
    (unless (or (search-forward (ett-convert-date date) nil t) date2)
      (kill-buffer "*ETT*")
      (user-error "Date was not found, aborting"))
    (ett-prepare-scope)
    (if (stringp date2)
        (when (search-forward (ett-convert-date date2) nil t)
          (forward-line 1)
          (when (re-search-forward "^[0-9]+-[0-9]+-[0-9]+" nil t)
            (beginning-of-line)
            (delete-region (point) (point-max)))
          (ett-finalize-scope "00:00"))
      (if (not (re-search-forward "^[0-9]+-[0-9]+-[0-9]+" nil t 2))
          (ett-finalize-scope (format-time-string "%H:%M"))
        (beginning-of-line)
        (delete-region (point) (point-max))
        (ett-finalize-scope "00:00")))))

(defun ett-prepare-scope ()
  "Clean everything from point to point-min.
When there's a previous track, don't delete it, and change
timestamp to 00:00."
  (beginning-of-line)
  (unless (bobp)
    (search-backward ett-separator nil t)
    (beginning-of-line)
    (delete-region (point) (save-excursion (forward-char 5) (point)))
    (insert "00:00"))
  (beginning-of-line)
  (delete-region (point) (point-min)))

(defun ett-finalize-scope (&optional timestamp)
  "Add a dumb track at the end of current buffer with current time.
This allows calculating times accurately even for unfinished tracks.
When TIMESTAMP is non-nil, use that as timestamp."
  (goto-char (point-max))
  (insert
   "\n" (or timestamp (format-time-string "%H:%M"))
   " " ett-separator " UglyDumbItem"))

(defun ett-item-at-point ()
  "Return buffer substring between point and next whitespace."
  (buffer-substring-no-properties
   (point)
   (save-excursion
     (skip-chars-forward "^ " (line-end-position))
     (point))))

(defun ett-parse-time (&optional nolimit)
  "Return time at point as a list of hours and minutes.
When NOLIMIT is non-nil, don't stick with HH:MM; rather,
allow HHH:MM and others."
  (let ((point (point))
        hr mn)
    ;; I used to use number-at-point, but it was too slow.
    ;; Remember, ETT file can have thousands of lines.
    (setq hr (string-to-number
              (if nolimit
                  (buffer-substring
                   point
                   (progn (skip-chars-forward "0-9") (point)))
                (buffer-substring point (+ point 2))))
          mn (string-to-number
              (if nolimit
                  (buffer-substring
                   (+ (point) 1)
                   (+ (point) 3))
                (buffer-substring (+ point 3) (+ point 5)))))
    ;; We are done with this timestamp, so go to end of line
    (end-of-line)
    ;; Return time
    (list hr mn)))

(defun ett-add-complex-statistics (item scope &optional fdate)
  "Add complex statistics for ITEM on SCOPE.
FDATE can delimit scope."
  (let ((daycount 0)
        daylist total daytimelist cdate sum max min)
    (with-temp-buffer
      (insert-file-contents ett-file)
      (pcase scope
        ('day
         (ett-helper
          "%d-%m-%y"
          "Error: current day wasn't found; maybe call ett-add-track?"))
        ('month (ett-helper "01-%m-%y"))
        ('year (ett-helper "01-01-%y"))
        ('week (ett-helper "%d-%m-%y" nil :day -6))
        (_ (ett-custom-date scope fdate)))
      (save-excursion
        (while (re-search-forward "^[0-9]+-[0-9]+-[0-9]+$" nil t)
          (setq daycount (1+ daycount)
                daylist (append
                         (ensure-list
                          (buffer-substring
                           (line-beginning-position) (line-end-position)))
                         daylist))))
      (delete-region (point-min) (point-max))
      (while daylist
        (insert-file-contents ett-file)
        (setq cdate (car daylist))
        (ett-custom-date cdate)
        (ett-parse-buffer t)
        (setq total (or (alist-get item ett-items-internal nil nil #'equal) 0)
              daytimelist
              (append (list (cons cdate total)) daytimelist)
              daylist (delete cdate daylist))
        (delete-region (point-min) (point-max))))
    (with-current-buffer "*ETT*"
      (goto-char (point-min))
      (forward-line 1)
      (insert "\nShowing statistics for item: " item "\n\n")
      (setq sum (seq-reduce #'+ (mapcar #'cdr daytimelist) 0))
      (insert "Total: " (ett-prettify-time sum) "\n\n")
      (unless (or (and (stringp scope) (not fdate))
                  (eq scope 'day))
        (insert "Average: "
                (ett-prettify-time
                 (round
                  (/ (float sum) (length (mapcar #'car daytimelist)))))
                " (per day)\n\n")
        (insert "Most time in a day: "
                (ett-prettify-time
                 (setq max (seq-reduce #'max (mapcar #'cdr daytimelist) 0)))
                " (on "
                (car (rassoc max daytimelist))
                ")\n\n")
        (insert "Less time in a day: "
                (ett-prettify-time
                 (setq min (seq-reduce #'min (mapcar #'cdr daytimelist) 10000)))
                " (on "
                (car (rassoc min daytimelist))
                ")\n\n")
        (unless (eq max 0) (ett-insert-graph max daytimelist))
        (insert "\n\n\nTags report:")))))

(defun ett-insert-rectangle (rectangle)
  "Insert text of RECTANGLE with upper left corner at point.
Like `insert-rectangle', but doesn't set mark."
  (let ((lines rectangle)
	(insertcolumn (current-column))
	(first t)
        indent-tabs-mode)
    (while lines
      (or first
	  (progn
	   (forward-line 1)
	   (or (bolp) (insert ?\n))
	   (move-to-column insertcolumn t)))
      (setq first nil)
      (insert (car lines))
      (setq lines (cdr lines)))))

(defun ett-date< (element1 element2)
  "Return non-nil if date on ELEMENT1 is less than date on ELEMENT2."
  (let ((first (car element1))
        (second (car element2)))
    (cond ((string= (substring first 6 8) (substring second 6 8))
           (cond ((string= (substring first 3 5)
                           (substring second 3 5))
                  (if (string< (substring first 0 2) (substring second 0 2))
                      t nil))
                 ((string< (substring first 3 5) (substring second 3 5))
                  t)
                 (t nil)))
          ((string< (substring first 6 8) (substring second 6 8))
           t)
          (t nil))))

(defun ett-insert-graph (max list)
  "Insert a graph for tracks on LIST depending on MAX."
  (let* ((graph ett-graph-height)
         (graph-n (/ (float graph) max))
         (col 0)
         current first last beacon middle pad graph-f max-pret)
    (insert "\n\n" (make-string (- graph 1) ?\n))
    ;; Sort list
    (setq list (sort list #'ett-date<))
    ;; Store some values for later
    (setq first (car (car list))
          last (car (car (last list))))
    ;; Now add them, one by one
    (while list
      (setq current (car list)
            graph-f (round (* graph-n (cdr current))))
      (if (>= 0 graph-f)
          (insert " ")
        (forward-line (- 1 graph-f))
        (end-of-line)
        (unless (eq (current-column) col)
          (insert
           (mapconcat #'concat (make-list (- col (current-column)) " ") "")))
        (ett-insert-rectangle
         (make-list graph-f (string ett-graph-icon))))
      (setq list (cdr list)
            col (current-column)))
    (setq beacon (point)
          max-pret (ett-prettify-time max)
          pad (length max-pret))
    (insert "\n\n")
    (goto-char beacon)
    (forward-line (- 0 graph))
    (ett-insert-rectangle
     (flatten-list
      (list
       max-pret
       (make-list
        (- (/ graph 2) 1)
        (make-string (+ pad 1) ? ))
       (concat
        (setq middle (ett-prettify-time (round (/ (float max) 2))))
        (make-string (- pad (length middle) -1) ? ))
       (make-list
        (- (/ graph 2) 1)
        (make-string (+ pad 1) ? ))
       (concat
        "0m"
        (make-string (- pad 1) ? )))))
    (forward-line)
    (insert
     (make-string (+ pad 1) ? )
     (make-string col ?-))
    (insert "\n\n(From " first " to " last ")")))

(defun ett-prettify-time (time)
  "Return a prettified string for TIME."
  (let ((hr (floor time 60))
        (mn (% time 60)))
    (concat
     (if (> hr 0) (format "%dh " hr) "")
     (format "%dm" (or mn 0)))))

(defun ett-calculate-time-difference (time1 time2)
  "Calculate time difference between TIME1 and TIME2.
These both must be lists on the form (HOURS MINUTES)."
  (let (hr mn)
    ;; Calculate difference between hours and minutes
    (setq hr (- (nth 0 time2) (nth 0 time1))
          mn (- (nth 1 time2) (nth 1 time1)))
    ;; If hours are negative, add 24
    (if (> 0 hr) (setq hr (+ 24 hr)))
    ;; If minutes are negative, add 60 and substract an hour
    (if (> 0 mn)
        (setq mn (+ 60 mn)
              hr (- hr 1)))
    ;; Now convert to minutes only and return that
    (while (> hr 0)
      (setq mn (+ 60 mn)
            hr (- hr 1)))
    mn))

(defun ett-parse-buffer (&optional all)
  "Parse every ETT item on current buffer.
When ALL is non-nil, also parse items on `ett-excluded-items'."
  (let (ctime ntime item total onlist)
    (save-excursion
      (setq ett-items-internal nil)
      (goto-char (point-min))
      (while (save-excursion (search-forward ett-separator nil t))
        ;; There's at least one ETT item to parse, so jump to it.
        (search-forward ett-separator)
        ;; We now collect item name.
        ;; As per specifications, whitespace(s) separes us from it.
        (skip-chars-forward " ")
        ;; Set item.
        ;; (I used to use word-at-point, but it was too slow)
        (setq item (ett-item-at-point))
        ;; We now want to collect times.
        ;; As per specifications, they are at beginning of line.
        (beginning-of-line)
        ;; We name this time 'ctime' (current time).
        (setq ctime (ett-parse-time))
        ;; We have our first time. Go to next timestamp.
        ;; We save excursion, as we want to come back here later.
        (save-excursion
          (search-forward ett-separator nil t)
          ;; We don't care about what item this is. Just collect time.
          (beginning-of-line)
          ;; We name this time 'ntime' (next time).
          (setq ntime (ett-parse-time)))
        ;; Now, we calculate how many time there is.
        (when (or all (not (member item ett-excluded-items)))
          (setq total (ett-calculate-time-difference ctime ntime)
                onlist (alist-get item ett-items-internal nil nil 'equal))
          ;; Add it to our list
          (if onlist
              ;; Increment value on list if already there
              (setf (alist-get item ett-items-internal nil nil 'equal)
                    (+ onlist total))
            ;; Add it otherwise
            (setq ett-items-internal
                  (append (list (cons item total)) ett-items-internal))))))))

(defun ett-parse-tags (item)
  "Parse current buffer so tag values for ITEM are collected."
  (let (ctime ntime tag total onlist item-reg)
    (save-excursion
      (setq ett-items-internal nil
            item-reg (concat ett-separator " +" item))
      (goto-char (point-min))
      (while (save-excursion (re-search-forward item-reg nil t))
        ;; There's at least one ETT item to parse, so jump to it.
        (re-search-forward item-reg)
        ;; We now collect tag.
        ;; As per specifications, whitespace(s) separes us from it.
        (skip-chars-forward " ")
        ;; Set tag.
        (if (not (looking-at "\\["))
            ;; We aren't looking at a tag. Use a "No tags" tag.
            (setq tag "No tags")
          ;; We are looking at a tag. Set it.
          (forward-char)
          (setq tag
                (buffer-substring
                 (point)
                 (save-excursion
                   (search-forward "]" (line-end-position))
                   (forward-char -1) (point)))))
        ;; We now want to collect times.
        ;; As per specifications, they are at beginning of line.
        (beginning-of-line)
        ;; We name this time 'ctime' (current time).
        (setq ctime (ett-parse-time))
        ;; We have our first time. Go to next timestamp.
        ;; We save excursion, as we want to come back here later.
        (save-excursion
          (search-forward ett-separator nil t)
          ;; We don't care about what item this is. Just collect time.
          (beginning-of-line)
          ;; We name this time 'ntime' (next time).
          (setq ntime (ett-parse-time)))
        ;; Now, we calculate how many time there is.
        (setq total (ett-calculate-time-difference ctime ntime)
              onlist (alist-get tag ett-items-internal nil nil 'equal))
        ;; Add it to our list
        (if onlist
            ;; Increment value on list if already there
            (setf (alist-get tag ett-items-internal nil nil 'equal)
                  (+ onlist total))
          ;; Add it otherwise
          (setq ett-items-internal
                (append (list (cons tag total)) ett-items-internal)))))))

(defun ett-parse-items-list ()
  "Parse current buffer so all ETT items are collected."
  (let (item)
    (save-excursion
      (setq ett-items-internal nil)
      (goto-char (point-min))
      (while (search-forward ett-separator nil t)
        ;; There's at least one ETT item to parse. We now collect it.
        ;; As per specifications, whitespace(s) separes us from it.
        (skip-chars-forward " ")
        ;; Set item.
        ;; (I used to use word-at-point, but it was too slow)
        (setq item (ett-item-at-point))
        ;; We don't care about times, we just want an item list.
        ;; So, store it right away.
        (unless (member item ett-items-internal)
          (setq ett-items-internal (cons item ett-items-internal)))))))

(defun ett-parse-item (item)
  "Parse current buffer so total time for ITEM is collected."
  (let (ctime ntime total onlist item-reg)
    (save-excursion
      (setq ett-items-internal nil
            item-reg (concat ett-separator " +" item))
      (goto-char (point-min))
      (while (save-excursion (re-search-forward item-reg nil t))
        ;; There's at least one ETT track to parse, so jump to it.
        (re-search-forward item-reg)
        ;; We only want to collect times.
        ;; As per specifications, they are at beginning of line.
        (beginning-of-line)
        ;; We name this time 'ctime' (current time).
        (setq ctime (ett-parse-time))
        ;; We have our first time. Go to next timestamp.
        ;; We save excursion, as we want to come back here later.
        (save-excursion
          (search-forward ett-separator nil t)
          ;; We don't care about what item this is. Just collect time.
          (beginning-of-line)
          ;; We name this time 'ntime' (next time).
          (setq ntime (ett-parse-time)))
        ;; Now, we calculate how many time there is.
        (setq total (ett-calculate-time-difference ctime ntime)
              onlist (alist-get item ett-items-internal nil nil 'equal))
        ;; Add it to our list
        (if onlist
            ;; Increment value on list if already there
            (setf (alist-get item ett-items-internal nil nil 'equal)
                  (+ onlist total))
          ;; Add it otherwise
          (setq ett-items-internal
                (append (list (cons item total)) ett-items-internal)))))))

(defun ett-return-items ()
  "Return a list with all items on ETT file."
  (with-current-buffer (find-file-noselect ett-file)
    (ett-parse-items-list)
    ;; Return value
    ett-items-internal))

(defun ett-insert-time (item)
  "Write collected times for ITEM on current buffer."
  (let ((mn (alist-get item ett-items-internal nil nil #'equal))
        (hr 0))
    (while (>= mn 60)
      (setq hr (1+ hr)
            mn (- mn 60)))
    (unless (= hr mn 0)   ; Don't act if we have a 0
      (setq hr (number-to-string hr)
            mn (number-to-string mn))
      (insert item " " ett-pretty-separator " "
              (if (length= hr 1) "0" "")
              hr
              (if (length= mn 2) ":" ":0")
              mn "\n"))))

(defun ett-calculate-percentage (&optional nototal)
  "Add a percentage value next to each track.
When NOTOTAL is non-nil, don't add total indicator."
  (let ((total 0))
    (goto-char (point-min))
    (save-excursion
      (while (re-search-forward "[0-9]+:" nil t)
        (goto-char (match-beginning 0))
        (let ((num (ett-parse-time t)))
          (setq total (+ (nth 1 num) (* (nth 0 num) 60) total)))))
    (save-excursion
      (while (re-search-forward "[0-9]+:" nil t)
        (goto-char (match-beginning 0))
        (let ((num (ett-parse-time t))
              percentage)
          (setq percentage
                (number-to-string
                 (round (* 100
                           (/
                            (float (+ (* (nth 0 num) 60) (nth 1 num)))
                            total)))))
          (insert
           "   ["
           (if (length= percentage 1)
               (concat " " percentage)
             percentage)
           "%]"))))
    (goto-char (point-min))
    (while (search-forward "[ 0%]" nil t)
      (replace-match "[<1%]" nil nil))
    (unless nototal
      (goto-char (point-max))
      (insert "\n\nTotal: " (ett-prettify-time total)))))

(defun ett-sort ()
  "Sort all tracks on current buffer, and align items."
  (let (indent-tabs-mode)
    (goto-char (point-min))
    (save-excursion
      ;; Handle three digits tracks
      (when (re-search-forward " [0-9]\\{3\\}:" nil t)
        (goto-char (point-min))
        (while (re-search-forward " [0-9]\\{2\\}:" nil t)
          (goto-char (match-beginning 0))
          (forward-char)
          (insert "0")
          (end-of-line))))
    (save-excursion
      ;; Sort items
      ;; We need to do it in two steps because it's somehow complicated
      (sort-regexp-fields t "^.*$" "\\(:[0-9]+\\)" (point-min) (point-max))
      (sort-regexp-fields t "^.*$" "\\([0-9]+:\\)" (point-min) (point-max))
      ;; We now align
      (align-regexp
       (point-min) (point-max) (concat "\\(\\s-*\\)" ett-pretty-separator)))
    (save-excursion
      ;; We now have aligned and cute items
      ;; Except for those trailing zeros...
      (while (re-search-forward " 0[0-9]\\{2\\}:" nil t)
        (goto-char (match-beginning 0))
        (forward-char)
        (delete-char 1)
        (insert " ")
        (end-of-line)))))

(defun ett-add-icon (&optional tag)
  "Add an icon for every track.
When TAG is non-nil, just do it for TAG."
  (when (and ett-use-icons (display-graphic-p))
    (require 'all-the-icons)
    (goto-char (point-min))
    (if (not tag)
        (while (search-forward "%]" nil t)
          (beginning-of-line)
          (when-let ((icon (assoc-string (ett-item-at-point) ett-icon-alist)))
            (insert (funcall ett-icon-function (cdr icon) :height 0.9) " "))
          (end-of-line))
      (search-forward tag)
      (goto-char (match-beginning 0))
      (when-let ((icon (assoc-string (ett-item-at-point) ett-icon-alist)))
        (insert " " (funcall ett-icon-function (cdr icon) :height 0.9) " ")))
    (forward-line 1)))

(defun ett-api (item &optional date1 date2)
  "Return ETT total time for ITEM.
DATE1 and DATE2, which are strings on the form DD-MM-YY, can be
used to delimit scope.
DATE1 can also be a symbol such as day, month, week or year."
  (catch 'exit
    (with-temp-buffer
      (insert-file-contents ett-file)
      (when (stringp date1)
        (condition-case nil (ett-custom-date date1 date2)
          (t (throw 'exit 0))))
      (when (and (symbolp date1) (not (null date1)))
        (pcase date1
          ('day
           (ett-helper
            "%d-%m-%y"
            "Error: current day wasn't found; maybe call ett-add-track?"))
          ('month (ett-helper "01-%m-%y"))
          ('year (ett-helper "01-01-%y"))
          ('week (ett-helper "%d-%m-%y" nil :day -6))
          (_ (throw 'exit 0))))
      (ett-parse-item item)
      (or (alist-get item ett-items-internal nil nil 'equal) 0))))

(defun ett-last-tracked-at (item)
  "Create a new buffer showing last tracks of ITEM."
  (interactive (list (ett-complete "Item: " 'items)))
  (let ((inhibit-read-only t))
    (with-current-buffer (get-buffer-create "*ETT-log*")
      (delete-region (point-min) (point-max))
      (insert-file-contents ett-file)
      (keep-lines
       (concat "\\(" (regexp-quote item) "\\|[0-9]+-[0-9]+-[0-9]\\)"))
      (goto-char (point-min))
      (forward-line)
      (while (re-search-forward "^[0-9]+-[0-9]+-[0-9]+$" nil t)
        (beginning-of-line)
        (insert "\n")
        (forward-line)))
    (switch-to-buffer "*ETT-log*")
    (ett-mode)
    (read-only-mode)
    (goto-char (- (point-max) 1))))

(defun ett-report-item (item scope)
  "Display statistics for ITEM on SCOPE."
  (interactive
   (list
    (ett-complete "Item: " 'items)
    (completing-read "Period: " '("Week" "Month" "Day" "Year" "Custom period"))))
  (let (finaldate)
    (setq scope
          (pcase scope
            ("Week" 'week)
            ("Month" 'month)
            ("Day" 'day)
            ("Year" 'year)
            ("Custom period"
             (prog1 (read-string "Date 1 (DD-MM-YY): ")
               (setq finaldate (read-string "Date 2 (DD-MM-YY): "))))
            (_ scope)))
    (ett-report scope item finaldate)))

(defun ett-helper (string &optional error-message time1 time2 dumbtime)
  "Prepare current buffer for ETT statistic generation.
STRING is the first date we should track. This is passed to
`format-time-string', so it can have placeholders such as %y
for current year.
With ERROR-MESSAGE, signal an error when STRING is not found. By
default, this function silently ignores that scenario.
Optional TIME1 and TIME2 changes how placeholders are replaced.
For instance, if TIME1 is the symbol :day and TIME2 is the
integer -7, %d in STRING will be replaced by the day that was current
exactly one week ago. These arguments are passed to `make-decoded-time'.
DUMBTIME is passed to `ett-finalize-scope'."
  (goto-char (point-min))
  (let (desired-date)
    (save-excursion
      (when (and time1 time2)
        (let* ((today (decode-time))
               (last-date-1
                (decoded-time-add
                 today (funcall #'make-decoded-time time1 time2)))
               (last-date (encode-time last-date-1)))
          (setq desired-date last-date)))
      (cond ((search-forward (format-time-string string desired-date) nil t)
             (ett-prepare-scope)
             (ett-finalize-scope dumbtime))
            (error-message (error error-message))
            (t (ett-finalize-scope dumbtime))))))

;;;###autoload
(defun ett-report (scope &optional item finaldate)
  "Report statistics for SCOPE.
SCOPE can be a symbol (day, month, year or week) or
a custom date as a string (e.g. 11-02-22).
With ITEM, report only statistics for ITEM.
With FINALDATE, use it to delimit SCOPE."
  (interactive (list 'day))
  (let ((inhibit-read-only t))
    (with-current-buffer (get-buffer-create "*ETT*")
      (erase-buffer)
      ;; First, we get the contents from our file
      (insert-file-contents ett-file)
      ;; For changing the scope of our tracking, we
      ;; just erase what we don't need
      (pcase scope
        ('day
         (ett-helper
          "%d-%m-%y"
          "Error: current day wasn't found; maybe call ett-add-track?"))
        ('month (ett-helper "01-%m-%y"))
        ('year (ett-helper "01-01-%y"))
        ('week (ett-helper "%d-%m-%y" nil :day -6))
        (_ (ett-custom-date scope finaldate)))
      ;; We now parse our buffer
      (if item
          (ett-parse-tags item)
        (ett-parse-buffer))
      ;; We no longer need whatever is here
      (delete-region (point-min) (point-max))
      ;; Now we write our values
      (mapc #'ett-insert-time (mapcar #'car ett-items-internal))
      ;; Prepare view
      (ett-sort)
      (insert
       (cond ((equal scope 'day)
              "This day:")
             ((equal scope 'week)
              "This week:")
             ((equal scope 'month)
              "This month:")
             ((equal scope 'year)
              "This year:")
             (finaldate
              (format "Period:  %s -- %s"
                      (ett-convert-date scope) (ett-convert-date finaldate)))
             (t (format "Date:  %s" (ett-convert-date scope))))
       "\n\n")
      ;; Add percentage
      (ett-calculate-percentage item)
      ;; Add statistics (graph, etc.)
      (when item (ett-add-complex-statistics item scope finaldate))
      ;; Add goal indication
      (when (and ett-goals (not item)) (ett-add-goals scope))
      ;; Add icons
      (ett-add-icon item))
    ;; Finish
    (switch-to-buffer "*ETT*")
    (ett-view-mode)
    (read-only-mode)
    (goto-char (point-min))))

;; Mode line indicator code
(defvar ett-modeline-display-string "")
(defvar ett-timer nil)

(defvar ett-modeline-mode nil
  "Non-nil if `ett-modeline-mode' is enabled.
Use the command `ett-modeline-mode' to change this variable.")

(defun ett-modeline-mode ()
  "Toggle display of ETT timer on mode line."
  (interactive)
  (when ett-timer (cancel-timer ett-timer))
  (or global-mode-string (setq global-mode-string '("")))
  (if ett-modeline-mode
      (progn
        (setq ett-modeline-display-string ""
              ett-modeline-mode nil)
        (remove-hook 'ett-clock-in-hook #'ett-timer))
    (or (memq 'ett-modeline-display-string global-mode-string)
        (setq global-mode-string
              (append global-mode-string '(ett-modeline-display-string))))
    (ett-timer)
    (setq ett-timer (run-at-time t 60 #'ett-timer)
          ett-modeline-mode t)
    (add-hook 'ett-clock-in-hook #'ett-timer))
  (force-mode-line-update))

(defun ett-timer ()
  "Update mode line with ETT time every minute."
  (let (last-time current-item current-time goal)
    (with-current-buffer (find-file-noselect ett-file)
      (save-excursion
        (goto-char (point-max))
        (search-backward ett-separator)
        (goto-char (match-end 0))
        (skip-chars-forward " ")
        (setq current-item (ett-item-at-point))
        (beginning-of-line)
        (setq last-time (ett-parse-time))))
    (setq current-time
          (list (string-to-number (format-time-string "%H"))
                (string-to-number (format-time-string "%M"))))
    (setq goal (ett-get-goals current-item 'day))
    (setq ett-modeline-display-string
          (format "[%s -- %s%s] "
                  current-item
                  (ett-prettify-time
                   (ett-calculate-time-difference
                    last-time current-time))
                  (if goal
                      (format
                       " | G: %s | T: %s"
                       (ett-prettify-time goal)
                       (ett-prettify-time (ett-api current-item 'day)))
                    "")))
    (when (eq goal (ett-api current-item 'day))
      (run-hook-with-args 'ett-daily-goal-reached-hook current-item))))

;; Goal support
(defun ett-get-goals (item period)
  "Get ITEM goals, in minutes, for PERIOD."
  (let ((goals (copy-alist ett-goals))
        minutes first-list)
    (while goals
      (setq first-list (car goals))
      (when
          (and (equal (car first-list) item)
               (eq (nth 1 first-list) period))
        (setq minutes (car (last first-list))))
      (setq goals (cdr goals)))
    minutes))

(defun ett-add-goals (scope)
  "Add goal reminders for SCOPE on current buffer."
  (let ((goals (copy-alist ett-goals))
        items temp-item item-goal item-ctime)
    (if (not (symbolp scope))
        nil
      (while goals
        (when (memq scope (car goals))
          (setq items (append (ensure-list (car (car goals))) items)))
        (setq goals (delete (car goals) goals)))
      (goto-char (point-min))
      (while items
        (save-excursion
          (setq temp-item (car items)
                item-goal (ett-get-goals temp-item scope)
                ;; ett-items-internal should hold current values right now.
                ;; At least, we assume that.
                item-ctime
                (alist-get temp-item ett-items-internal nil nil #'equal))
          (if (re-search-forward (concat "^" temp-item " ") nil t)
              (progn
                (end-of-line)
                (insert
                 (if (>= item-ctime item-goal)
                     "   [Goal reached]"
                   (concat
                    "   ["
                    (ett-prettify-time (- item-goal item-ctime))
                    " remaining for reaching goal]"))))
            (goto-char (point-max))
            (unless (save-excursion
                      (re-search-backward "^Pending goals:" nil t))
              (insert
               "\n\n\nPending goals:\n\n"))
            (insert
             "- " temp-item " ("
             (ett-prettify-time (ett-get-goals temp-item scope)) ")\n"))
          (setq items (delete temp-item items)))))))

(defun ett-report-year ()
  "Report statistics for current year."
  (interactive)
  (ett-report 'year))

(defun ett-report-month ()
  "Report statistics for current month."
  (interactive)
  (ett-report 'month))

(defun ett-report-week ()
  "Report statistics for current week."
  (interactive)
  (ett-report 'week))

(defun ett-report-period ()
  "Report statistics for a period."
  (interactive)
  (ett-choose-custom-date t))

(transient-define-prefix ett-choose-view ()
  "Choose desired view for report."
  ["Available views:\n"
   [("y" "Year" ett-report-year)
    ("m" "Month" ett-report-month)
    ("w" "Week" ett-report-week)
    ("d" "Day" ett-report)
    ("c" "Custom date" ett-choose-custom-date)
    ("p" "Custom period" ett-report-period)
    ("i" "Detailed stats for item" ett-report-item)]])

(defun ett-sanity-check ()
  "Try to find an error on .ett file."
  (interactive)
  (let (time-1 time-2)
    (goto-char (point-min))
    (catch 'exit
      (while (search-forward ett-separator nil t)
        (beginning-of-line)
        (setq time-1 (ett-parse-time))
        (search-forward ett-separator nil t)
        (beginning-of-line)
        (setq time-2 (ett-parse-time))
        (save-excursion
          (if (not (search-forward ett-separator nil t)) (throw 'exit t)))
        (beginning-of-line)
        (when (and (= (car time-1) (car time-2))
                   (> (nth 1 time-1) (nth 1 time-2)))
          (user-error "Look here"))
        (when (and (> (car time-1) (car time-2))
                   (save-excursion
                     (forward-line -1)
                     (looking-at "[0-9]+:")))
          (user-error "Look here"))))
    (message "No errors")))

(defun ett-choose-custom-date (&optional period)
  "Let the user choose a custom date, or period if PERIOD is non-nil."
  (interactive)
  (save-window-excursion
    (calendar)
    (if (not period)
        (ett-report (read-string "Date (DD-MM-YY): "))
      (ett-report
       (read-string "Date 1 (DD-MM-YY): ") nil
       (read-string "Date 2 (DD-MM-YY): ")))))

(transient-define-suffix ett-transient-delete-track ()
  "Delete last ETT track."
  (interactive (list (transient-args transient-current-command)))
  (with-current-buffer (find-file-noselect ett-file)
    (goto-char (point-max))
    (re-search-backward ett-separator nil t)
    (delete-region (line-beginning-position) (line-beginning-position 2))
    (save-buffer 0)
    (message "Deleted track")))

(transient-define-suffix ett-transient-add-track (&optional args)
  "Add a track to ETT from transient."
  (interactive (list (transient-args transient-current-command)))
  (let (item tag)
    (when args
      (when (member ett-transient-for-tag args)
        (delete ett-transient-for-tag args)
        (setq item (seq-reduce #'concat args ""))
        (setq tag (ett-complete "Tag: " item)))
      (or item (setq item (seq-reduce #'concat args ""))))
    ;; Clock in
    (ett-add-track item tag)))

(defvar ett-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "C-c C-c") #'ett-add-track)
    (define-key map (kbd "SPC") #'ett-space-dwim)
    map))

(define-derived-mode ett-mode text-mode "ETT"
  "Major mode for editing ETT based texts."
  (setq font-lock-defaults '(ett-font-lock-keywords t))
  (setq-local outline-regexp "[0-9]+-[0-9]+-[0-9]+")
  ;; show-paren-mode can be somehow annoying here
  (show-paren-local-mode -1))

(defvar ett-view-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "q") #'bury-buffer)
    (define-key map (kbd "SPC") #'ett-choose-view)
    (define-key map (kbd "<up>") #'scroll-down-command)
    (define-key map (kbd "<down>") #'scroll-up-command)
    map))

(define-derived-mode ett-view-mode ett-mode "ETT [report]"
  "Major mode for viewing ETT reports."
  (setq font-lock-defaults '(ett-view-font-lock-keywords t))
  (put-text-property 1 (point-max) 'line-prefix "   ")
  (setq-local view-read-only nil)
  (setq cursor-type nil))

;;;###autoload
(add-to-list 'auto-mode-alist '("\\.ett\\'" . ett-mode))

(provide 'ett)

;;; ett.el ends here
